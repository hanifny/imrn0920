import React, {Component} from 'react';
import Constants from 'expo-constants';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconFA from 'react-native-vector-icons/FontAwesome5';
import data from '../skillData.json';
import SkillCard from './SkillCard';

export default class About extends Component {
    render() {
        let skill = this.props.skill
        return (
            <View style={styles.container}>
                <View style={styles.skillCard}>
                    <IconFA size={57} color='#003366' name={ skill.iconName } solid />
                    <View>
                        <Text style={{fontSize: 24, color: '#003366'}}>{skill.skillName}</Text>
                        <Text style={{fontSize: 16, color: '#3EC6FF'}}>{skill.categoryName}</Text>
                        <View style={{alignItems: 'center'}}>
                            <Text style={{fontSize: 48, color: '#FFFFFF',}}>{skill.percentageProgress}</Text>
                        </View>
                    </View>
                    <Icon name='keyboard-arrow-right' size={73} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    navBar: {
      padding: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      marginTop: Constants.statusBarHeight
    },
    body: {
      flex: 1,
      paddingHorizontal: 15,
    },
    logo: {
        width: 199,
        height: 57,
    },
    welcome: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 21
    },
    title: {
        fontSize: 31,
        color: '#003366'
    },
    line: {
        height: 4,
        backgroundColor: '#3EC6FF'
    },
    allTag: {
        flexDirection: 'row', 
        marginTop: 9, 
        justifyContent: 'space-between'
    },
    tag: {
        backgroundColor: '#B4E9FF', 
        padding: 7, 
        borderRadius: 8, 
        color: '#003366',
        fontSize: 11.3,
        marginBottom: 11
    },
    skill: {
        paddingTop: 9
    },
    skillCard: {
        backgroundColor: '#B4E9FF',
        height: 129,
        padding: 11,
        marginBottom: 11,
        borderRadius: 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
  });
  