import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native';

export default class About extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <Image style={styles.logo} source={require('../assets/sanbercode.png')} />
                    <View style={styles.input}>
                        <Text>Username</Text>
                        <TextInput style={styles.inputText} editable maxLength={40} />
                    </View>
                    <View style={styles.input}>
                        <Text>Password</Text>
                        <TextInput style={styles.inputText} editable maxLength={40} />
                    </View>
                    <TouchableOpacity>
                        <View style={styles.button}>
                            <Text style={{color: 'white'}}>Masuk</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{flexDirection: 'row'}}>
                        <Text>atau </Text>
                        <TouchableOpacity>
                            <Text style={{color: '#00ACEE'}}>Register</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    body: {
      flex: 1,
      paddingHorizontal: 7,
      justifyContent: 'center',
      alignItems: 'center'
    },
    logo: {
        width: 299,
        height: 97,
        marginBottom: 49
    },
    input: {
        marginTop: 9,
        width: 277,
    },  
    inputText: {
        borderColor: 'black',
        borderWidth: 1,
        height: 45,
        marginTop: 9
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 21,
        width: 81,
        height: 41,
        borderRadius: 7,
        backgroundColor: '#00ACEE',
    }
  });
  