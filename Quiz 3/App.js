import React from 'react'
import { View, Image, Dimensions } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { AuthContext } from './context'
import Icon from 'react-native-vector-icons/FontAwesome5'

import { SignUp } from './components/SignUpScreen'
import { SignIn } from './components/SignInScreen'
import Home from './components/HomeScreen'
import { Project } from './components/ProjectScreen'
import { Add } from './components/AddScreen'

const RootStack = createStackNavigator()
const RootStackScreen = ({ userToken }) => (
  <RootStack.Navigator headerMode='none'>
    {userToken ? (
      <RootStack.Screen name='App' component={TabsScreen} />
    ) : (
      <RootStack.Screen name='SignUp' component={AuthStackScreen} />
    )}
  </RootStack.Navigator>
)

const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
  <AuthStack.Navigator headerMode='none'>
    <AuthStack.Screen 
      name='SignUp' 
      component={SignUp}
      // options={{ title: 'Sign In' }}
    />
    <AuthStack.Screen 
      name='SignIn' 
      component={SignIn}
      // options={{ title: 'Sign In' }}
    />
  </AuthStack.Navigator>
) 

const Tabs = createBottomTabNavigator()
const TabsScreen = () => (
  <Tabs.Navigator tabBarOptions={{activeTintColor: '#F77866', style: {paddingBottom: 9, paddingTop: 9}}}>
    <Tabs.Screen name='Home' component={Home} options={{
      tabBarIcon: ({ focused }) => (
        <Icon name="home" color={focused ? '#F77866' : '#727C8E'} size={21} />
      ),
    }} />
    <Tabs.Screen name='Cart' component={Project} options={{
      tabBarIcon: ({ focused }) => (
        <Icon name="shopping-cart" color={focused ? '#F77866' : '#727C8E'} size={21} />
      ),
    }} />
    <Tabs.Screen name='Message' component={Add} options={{
      // tabBarLabel:({ focused, color })=>(<Text style={{color: focused ? '#F77866' : '#727C8E'}}>Add</Text>),
      tabBarIcon: ({ focused }) => (
        <Icon name="envelope" color={focused ? '#F77866' : '#727C8E'} size={21} />
      ),
    }} />
    <Tabs.Screen name='Profile' component={Add} options={{
      // tabBarLabel:({ focused, color })=>(<Text style={{color: focused ? '#F77866' : '#727C8E'}}>Add</Text>),
      tabBarIcon: ({ focused }) => (
        <Icon name="user" color={focused ? '#F77866' : '#727C8E'} size={21} />
      ),
    }} />
  </Tabs.Navigator>
)

export default () => {
  const [userToken, setUserToken] = React.useState(null)
  const [isLoading, setIsLoading] = React.useState(true)

  const authContext = React.useMemo(() => {
    return {
      signUp: () => {
        setIsLoading(false)
        setUserToken(null)
      },
      signIn: () => {
        setIsLoading(false)
        setUserToken('secret')
      },
      signOut: () => {
        setIsLoading(false)
        setUserToken(null)
      }
    }
  }, [])

  // Start loading
  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false)
    }, 1000)
  }, [])
  if(isLoading) {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
          <View style = {{
          borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
          width: Dimensions.get('window').width * 0.9,
          height: Dimensions.get('window').width * 0.9,
          backgroundColor:'rgba(33,31,101,0.1)',
          justifyContent: 'center',
          alignItems: 'center'
          }}>
            <Image source={require('./assets/sanbercode.png')} />
          </View>
        </View>
        <View style={{flexDirection: 'row', height: 5, width: 134, backgroundColor: 'black', marginVertical: 24, alignSelf: 'center', borderRadius: 7}} />
      </View>
    )
  }
  // End loading

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <RootStackScreen userToken={userToken} />
      </NavigationContainer>
    </AuthContext.Provider>
  )
}