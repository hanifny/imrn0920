import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Button } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { AuthContext } from '../context'

export const SignIn = () => {
    const { signIn } = React.useContext(AuthContext)
    return (
        <View style={styles.container}>
            <View style={styles.body}>
                <View style={{width: 277, paddingBottom: 19}}>
                    <Text style={{fontWeight: 'bold', fontSize: 35}}>Welcome Back</Text>
                    <Text style={{color: '#4D4D4D'}}>Sign in to continue</Text>
                </View>
                <View style={styles.input}>
                    <Text>Email</Text>
                    <TextInput placeholderTextColor='#4C475A' placeholder='hanifnuryanto21@gmail.com' style={styles.inputText} editable maxLength={40} />
                </View>
                <View style={styles.input}>
                    <Text>Password</Text>
                    <View style={styles.inputText}>
                        <TextInput placeholderTextColor='#4C475A' placeholder='***********' editable maxLength={40} />
                        <Icon color='#727C8E' style={{alignSelf: 'center'}} name='eye' size={15} />
                    </View>
                </View>
                <TouchableOpacity style={{marginTop: 21, width: 277}}>
                    <Button title="Sign In" onPress={() => signIn()} color='#F77866' />
                    <Text style={{color: '#4D4D4D', alignSelf: 'flex-end', marginTop: 21, color: '#0C0423'}}>Forgot Password? </Text>
                </TouchableOpacity>
                <Text style={{marginVertical: 21}}>- OR -</Text>
                <View style={{flexDirection:'row', justifyContent: 'space-between', width: 277}}>
                    <View style={styles.socialMedia}>
                        <Image source={require('../assets/fb.png')} style={{width: 25, height: 25}} />
                        <Text> Facebook</Text>
                    </View>
                    <View style={styles.socialMedia}>
                        <Image source={require('../assets/google.png')} style={{width: 25, height: 25}} />
                        <Text> Google</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    body: {
      flex: 1,
      paddingHorizontal: 7,
      justifyContent: 'center',
      alignItems: 'center',
    },
    logo: {
        width: 299,
        height: 97,
        marginBottom: 49
    },
    input: {
        marginTop: 9,
        width: 277,
    },  
    inputText: {
        borderColor: '#E6EAEE',
        borderBottomWidth: 1,
        height: 45,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    socialMedia: {
        alignItems: 'center', 
        paddingVertical: 9,
        paddingHorizontal: 21, 
        borderColor: '#E6EAEE', 
        borderWidth: 1, 
        flexDirection: 'row',
        borderRadius: 5
    }
  });
  