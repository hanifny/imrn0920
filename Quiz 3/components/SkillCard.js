import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconFA from 'react-native-vector-icons/FontAwesome5';

export default class About extends Component {
    render() {
        let skill = this.props.skill
        return (
            <View style={styles.container}>
                <View style={styles.skillCard}>
                    <IconFA size={57} color='#003366' name={ skill.iconName } solid />
                    <View>
                        <Text style={{fontSize: 24, color: '#003366'}}>{skill.skillName}</Text>
                        <Text style={{fontSize: 16, color: '#3EC6FF'}}>{skill.categoryName}</Text>
                        <View style={{alignItems: 'center'}}>
                            <Text style={{fontSize: 48, color: '#FFFFFF',}}>{skill.percentageProgress}</Text>
                        </View>
                    </View>
                    <Icon name='keyboard-arrow-right' size={73} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    skillCard: {
        backgroundColor: '#B4E9FF',
        height: 129,
        padding: 11,
        marginBottom: 11,
        borderRadius: 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
  });
  