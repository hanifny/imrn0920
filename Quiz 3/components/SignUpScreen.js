import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Button } from 'react-native'
import { AuthContext } from '../context'

export const SignUp = ({ navigation }) => {
    const { signUp } = React.useContext(AuthContext)
    return (
        <View style={styles.container}>
            <View style={styles.body}>
                <View style={{width: 277, paddingBottom: 19}}>
                    <Text style={{fontWeight: 'bold', fontSize: 35}}>Welcome</Text>
                    <Text style={{color: '#4D4D4D'}}>Sign up to continue</Text>
                </View>
                <View style={styles.input}>
                    <Text>Name</Text>
                    <TextInput placeholderTextColor='#4C475A'  placeholder='Hanif Nuryanto'  style={styles.inputText} editable maxLength={40} />
                </View>
                <View style={styles.input}>
                    <Text>Email</Text>
                    <TextInput placeholderTextColor='#4C475A' placeholder='hanifnuryanto21@gmail.com' style={styles.inputText} editable maxLength={40} />
                </View>
                <View style={styles.input}>
                    <Text>Phone Number</Text>
                    <TextInput placeholderTextColor='#4C475A' placeholder='+62 857 7498 7703' style={styles.inputText} editable maxLength={40} />
                </View>
                <View style={styles.input}>
                    <Text>Password</Text>
                    <TextInput placeholderTextColor='#4C475A' placeholder='***********' style={styles.inputText} editable maxLength={40} />
                </View>
                <TouchableOpacity style={{marginTop: 21, width: 277}}>
                    <Button title="Sign Up" onPress={() => navigation.push('SignIn')} color='#F77866' />
                </TouchableOpacity>
                <View style={{flexDirection: 'row', marginTop: 21}}>
                    <Text style={{color: '#4D4D4D'}}>Already have an account? </Text>
                    <Text style={{color: '#F77866'}}>Sign In</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    body: {
      flex: 1,
      paddingHorizontal: 7,
      justifyContent: 'center',
      alignItems: 'center',
    },
    logo: {
        width: 299,
        height: 97,
        marginBottom: 49
    },
    input: {
        marginTop: 9,
        width: 277,
    },  
    inputText: {
        borderColor: '#E6EAEE',
        borderBottomWidth: 1,
        height: 45,
    },
  });
  