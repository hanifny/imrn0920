import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export const Project = () => {
    return (
        <View style={styles.container}>
            <View style={styles.body}>
                <Text>Halaman Proyek</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    body: {
      flex: 1,
      paddingHorizontal: 7,
      justifyContent: 'center',
      alignItems: 'center'
    },
  });
  