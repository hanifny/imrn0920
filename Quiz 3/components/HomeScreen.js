import React, {Component} from 'react';
import Constants from 'expo-constants';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5'
import data from '../skillData.json'
import SkillCard from './SkillCard'
import { TextInput } from 'react-native-gesture-handler';

export default class About extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <View style={styles.search}>
                        <Icon name='search' size={21} color='#727C8E' />
                        <TextInput placeholder='Search Product' />
                        <Icon name='camera' size={21} color='#727C8E' />
                    </View>
                    <Icon name='bell' size={21} color='#727C8E' />
                </View>
                <View style={styles.body}>
                    <View style={styles.welcome}>
                        <Image style={styles.welcome} source={require('../assets/Slider.png')} />
                        {/* <Icon name='account-circle' color='#00ACEE' size={51} />
                        <View style={{paddingLeft: 7}}>
                            <Text style={{fontSize: 17}}>Hai, </Text>
                            <Text style={{fontWeight: 'bold', fontSize: 21, color: '#003366'}}>Hanif Nuryanto </Text>
                        </View> */}
                    </View>

                    {/* <View>
                        <Text style={styles.title}>SKILL</Text>
                        <View style={styles.line} />
                    </View>

                    <View style={styles.allTag}>
                        <Text style={styles.tag}>Library / Framework</Text>
                        <Text style={styles.tag}>Bahasa Pemrograman</Text>
                        <Text style={styles.tag}>Teknologi</Text>
                    </View>

                    <FlatList 
                        data={data.items}
                        renderItem={(skill)=><SkillCard skill={skill.item} />}
                        keyExtractor={(item)=>item.id}
                    /> */}

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    navBar: {
      padding: 1,
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginTop: Constants.statusBarHeight + 21,
      alignItems: 'center'
    },
    search: {
        borderRadius: 9,
        borderWidth: 1,
        borderColor: '#727C8E',
        padding: 5,
        width: 277,
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignItems: 'center'
    },
    body: {
      flex: 1,
      paddingHorizontal: 15,
    },
    logo: {
        width: 199,
        height: 57,
    },
    welcome: {
        flexDirection: 'row',
        paddingBottom: 21,
        marginTop: 17,
        width: 329,
        justifyContent: 'center',
        borderRadius: 7,
    },
    title: {
        fontSize: 31,
        color: '#003366'
    },
    line: {
        height: 4,
        backgroundColor: '#3EC6FF'
    },
    allTag: {
        flexDirection: 'row', 
        marginTop: 9, 
        justifyContent: 'space-between'
    },
    tag: {
        backgroundColor: '#B4E9FF', 
        padding: 7, 
        borderRadius: 8, 
        color: '#003366',
        fontSize: 11.3,
        marginBottom: 11
    },
  });
  