// B. Tugas Conditional
//  If-else

var nama = "John" 
var peran = ""
var welcome = "Selamat datang di Dunia Werewolf, "

if (nama === "" && peran === "") {
    jawabanKonsol = "Nama harus diisi!"
} else if (nama && peran === "") {
    jawabanKonsol = "Halo " + nama + ", Pilih peranmu untuk memulai game!"
} else if (nama && peran === "Penyihir") {
    jawabanKonsol = welcome + nama + "<br>Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!"
} else if (nama && peran === "Guard") {
    jawabanKonsol = welcome + nama + "<br>Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf."
} else if (nama && peran === "Werewolf") {
    jawabanKonsol = welcome + nama + "<br>Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!"
}

console.log(jawabanKonsol);

//  Switch Case

var hari = 21
var bulan = 5
var tahun = 1999

daftar_bulan = [
                "Januari", "Februari", "Maret", 
                "April", "Mei", "Juni", 
                "Juli", "Agustus", "September", 
                "Oktober", "November", "Desember"
                ]

switch (true) {
    case 0 < bulan && bulan < 13:
        hasil = hari + " " + daftar_bulan[bulan-1] + " " + tahun
        break
    default:
        hasil = "Input tidak valid!"
        break
}

console.log(hasil)