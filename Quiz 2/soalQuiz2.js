// 1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
class Score {
    constructor(subject, points, email) {
        this.subject = subject
        this.points = points
        this.email = email
    }
    average = () => {
        if(this.points.length !== "undefined") {
            let sum = 0
            for (let i = 0; i < this.points.length; i++) {
                sum += this.points[i]
            }
            let avg = sum / this.points.length
            return avg
        }
    }
}

// 2. SOAL Create Score (10 Poin + 5 Poin ES6)
/*
data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/
const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
    let dataScore = []
    let dataSearch = []
    for (let i = 1; i < data.length; i++) {
        dataScore.push({"email": data[i][0], "subject": data[0][1], "points": data[i][1]})
        dataScore.push({"email": data[i][0], "subject": data[0][2], "points": data[i][2]})
        dataScore.push({"email": data[i][0], "subject": data[0][3], "points": data[i][3]})
    }
    for (let j = 0; j < dataScore.length; j++) {
        if(subject === dataScore[j].subject.replace(/\s+/g, '')) {
            dataSearch.push(dataScore[j])
        }
    }
    console.log(dataSearch)
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

// 3. SOAL Recap Score (15 Poin + 5 Poin ES6)
/*
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour
*/

function recapScores(data) {
    for (let i = 1; i < data.length; i++) {
        grade = (data[i][1] + data[i][2] + data[i][3]) / 3
        console.log(`
            ${i}. Email: ${data[i][0]}
            Rata-rata: ${Math.round(grade * 10) / 10} 
            Predikat: ${grade > 90 ? "honour" : (grade > 80 ? "graduate" : "participant")}
        `)
    }
}
  
recapScores(data);