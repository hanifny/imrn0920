import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SignIn, Search, Home, Details, Search2, Profile, Splash } from './Screen';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import  { createDrawerNavigator } from '@react-navigation/drawer';

import { AuthContext } from './context';

const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
  <AuthStack.Navigator>
    <AuthStack.Screen 
      name='SignIn' 
      component={SignIn}
      options={{ title: 'Sign In' }} />
  </AuthStack.Navigator>
) 

const RootStack = createStackNavigator();
const RootStackScreen = ({ userToken }) => (
  <RootStack.Navigator headerMode='none'>
    {userToken ? (
      <RootStack.Screen name='App' component={DrawerScreen} options={{
        animationEnabled: false
      }} />
    ) : (
      <RootStack.Screen name='Auth' component={AuthStackScreen} options={{
        animationEnabled: false
      }} />
    )}
  </RootStack.Navigator>
)

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName="Home">
    <Drawer.Screen name='Home' component={TabsScreen} />
    <Drawer.Screen name='Profile' component={ProfileStackScreen} />
  </Drawer.Navigator>
)

const HomeStack = createStackNavigator();
const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name='Home' component={Home} />
    <HomeStack.Screen name='Details' component={Details} options={({ route }) => ({
      title: route.params
    })} />
  </HomeStack.Navigator>
)

const SearchStack = createStackNavigator();
const SearchStackScreen = () => (
  <SearchStack.Navigator>
    <SearchStack.Screen name='Search' component={Search} />
    <SearchStack.Screen name='Search2' component={Search2} />
  </SearchStack.Navigator>
)

const ProfileStack = createStackNavigator();
const ProfileStackScreen = () => (
  <ProfileStack.Navigator>
    <ProfileStack.Screen name='Profile' component={Profile} /> 
  </ProfileStack.Navigator>
)

const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name='Home' component={HomeStackScreen} />
    <Tabs.Screen name='Search' component={SearchStackScreen} />
  </Tabs.Navigator>
)

export default () => {
  const [userToken, setUserToken] = React.useState(null)
  const [isLoading, setIsLoading] = React.useState(true)

  const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setIsLoading(false)
        setUserToken('asdf')
      },
      signUp: () => {
        setIsLoading(false)
        setIsLoading('asdf')
      },
      signOut: () => {
        setIsLoading(false)
        setUserToken(null)
      }
    }
  }, [])

  // Loading
  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false) 
    }, 1000)
  }, [])

  if (isLoading) {
    return <Splash />
  }
  // End loading


  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <RootStackScreen userToken={userToken} />
      </NavigationContainer>
    </AuthContext.Provider>
  )
}