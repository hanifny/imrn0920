import React from "react";
import { View, Text, StyleSheet, Button, Alert } from "react-native";
import { AuthContext } from "./context";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    borderRadius: 5
  }
});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Home = ({ navigation }) => {
  const { signOut } = React.useContext(AuthContext)
  return (
    <ScreenContainer>
      <Text>Master List Screen</Text>
      <Button title="React Native by Example" onPress={() => navigation.push('Details', ('React Native by Example'))} />
      <Button title="React Native School" onPress={() => navigation.push('Details', ('React Native School'))} />
      <Button title="Drawer" onPress={() => navigation.toggleDrawer()} />
      <Button color="red" title="Sign out" onPress={() => signOut()} />
    </ScreenContainer>
  )
}

export const Details = ({ route }) => {
  const { name } = route.params;
  return (
  <ScreenContainer>
    <Text>Details Screen</Text>
    <Text>{JSON.stringify(name)}</Text>
  </ScreenContainer>
  )
}

export const Search = ({ navigation }) => (
  <ScreenContainer>
    <Text>Search Screen</Text>
    <Button title="Search 2" onPress={() => navigation.push('Search2')} />
    <Button title="React Native School" onPress={() => {
      navigation.navigate('Details', 'React Native School')
    }} />
  </ScreenContainer>
);

export const Search2 = () => (
  <ScreenContainer>
    <Text>Search2 Screen</Text>
  </ScreenContainer>
);

export const Profile = ({ navigation }) => {
  const { signOut } = React.useContext(AuthContext)
  return (
    <ScreenContainer>
      <Text>Profile Screen</Text>
      <Button title="Drawer" onPress={() => navigation.toggleDrawer()} />
      <Button color="red" title="Sign Out" onPress={() => signOut()} />
    </ScreenContainer>
  );
};

export const Splash = () => (
  <ScreenContainer>
    <Text>Loading...</Text>
  </ScreenContainer>
);

export const SignIn = () => {
  const { signIn } = React.useContext(AuthContext)
  return (
    <ScreenContainer>
      <Text>Sign In Screen</Text>
      <Button title="Sign In" onPress={() => signIn()} />
    </ScreenContainer>
  );
};

