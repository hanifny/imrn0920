import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Button } from 'react-native'
import { AuthContext } from './context'

export const SignIn = () => {
    const { signIn } = React.useContext(AuthContext)
    return (
        <View style={styles.container}>
            <View style={styles.body}>
                <Image style={styles.logo} source={require('../../assets/sanbercode.png')} />
                <View style={styles.input}>
                    <Text>Username</Text>
                    <TextInput style={styles.inputText} editable maxLength={40} />
                </View>
                <View style={styles.input}>
                    <Text>Password</Text>
                    <TextInput style={styles.inputText} editable maxLength={40} />
                </View>
                <TouchableOpacity style={{marginTop: 21}}>
                    <Button title="SignIn" onPress={() => signIn()} />
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
    },
    body: {
      flex: 1,
      paddingHorizontal: 7,
      justifyContent: 'center',
      alignItems: 'center'
    },
    logo: {
        width: 299,
        height: 97,
        marginBottom: 49
    },
    input: {
        marginTop: 9,
        width: 277,
    },  
    inputText: {
        borderColor: 'black',
        borderWidth: 1,
        height: 45,
        marginTop: 9
    },
  });
  