import React from 'react'
import { View, Text } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { AuthContext } from './context'
import Icon from 'react-native-vector-icons/FontAwesome5'

import { SignIn } from './LoginScreen'
import  About from './AboutScreen'
import Skill from './SkillScreen'
import { Project } from './ProjectScreen'
import { Add } from './AddScreen'

const RootStack = createStackNavigator()
const RootStackScreen = ({ userToken }) => (
  <RootStack.Navigator headerMode='none'>
    {userToken ? (
      <RootStack.Screen name='App' component={DrawerScreen} />
    ) : (
      <RootStack.Screen name='SignIn' component={SignIn} />
    )}
  </RootStack.Navigator>
)

const Drawer = createDrawerNavigator()
const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName='Login'>
    <Drawer.Screen name='Skill' component={TabsScreen} />
    <Drawer.Screen name='About' component={About} />
  </Drawer.Navigator>
)

const Tabs = createBottomTabNavigator()
const TabsScreen = () => (
  <Tabs.Navigator tabBarOptions={{activeTintColor: '#003366', style: {paddingBottom: 7, paddingTop: 7}}}>
    <Tabs.Screen name='Skill' component={Skill} options={{
      tabBarIcon: ({ focused }) => (
        <Icon name="cog" color={focused ? '#003366' : '#B4E9FF'} size={21} />
      ),
    }} />
    <Tabs.Screen name='Project' component={Project} options={{
      tabBarIcon: ({ focused }) => (
        <Icon name="tasks" color={focused ? '#003366' : '#B4E9FF'} size={21} />
      ),
    }} />
    <Tabs.Screen name='Add' component={Add} options={{
      // tabBarLabel:({ focused, color })=>(<Text style={{color: focused ? '#003366' : '#B4E9FF'}}>Add</Text>),
      tabBarIcon: ({ focused }) => (
        <Icon name="plus" color={focused ? '#003366' : '#B4E9FF'} size={21} />
      ),
    }} />
  </Tabs.Navigator>
)

export default () => {
  const [userToken, setUserToken] = React.useState(null)
  const [isLoading, setIsLoading] = React.useState(true)

  const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setIsLoading(false)
        setUserToken('secret')
      },
      signOut: () => {
        setIsLoading(false)
        setUserToken(null)
      }
    }
  }, [])

  // Start loading
  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false)
    }, 1000)
  }, [])
  if(isLoading) {
    return (
      <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
        <Text>Loading...</Text>
      </View>
    )
  }
  // End loading

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <RootStackScreen userToken={userToken} />
      </NavigationContainer>
    </AuthContext.Provider>
  )
}