var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let i = 0
let time = 10000
async function baca(time) {
    if (i < books.length) {
        const x = await readBooksPromise(time, books[i])
        i++
        baca(x)
    }
}

baca(time) 