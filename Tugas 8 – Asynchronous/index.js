// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let i = 0
function sisaWaktu(check) {
    if(check > 2000 && i < books.length-1) {
        i++
        readBooks(check, books[i], sisaWaktu)
    } 
}   
readBooks(10000, books[i], sisaWaktu) 