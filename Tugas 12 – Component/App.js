import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList } from 'react-native';
import Constants from 'expo-constants'
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './app/components/videoItem';
import data from './app/data.json'

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
          <Image source={require('./app/images/logo.png')} style={{width: 98, height: 22}} />
          <View style={styles.rightNav}>
            <TouchableOpacity>
              <Icon style={styles.navItem} name="search" size={25} />
            </TouchableOpacity>

            <TouchableOpacity>
              <Icon style={styles.navItem} name="account-circle" size={25} />
            </TouchableOpacity>
          </View>
      </View>
      <View style={styles.body}>
        <FlatList 
          data={data.items}
          renderItem={(video)=><VideoItem video={video.item} />}
          keyExtractor={(item)=>item.id}
          ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor: "#E5E5E5"}}/>}
        />
      </View>
      <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tabItem}>
          <Icon color='red' name="home" size={21} />
          <Text style={styles.tabTitleClicked}> Home </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon color='#3c3c3c' name="whatshot" size={21} />
          <Text style={styles.tabTitle}> Trending </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon color='#3c3c3c' name="subscriptions" size={21} />
          <Text style={styles.tabTitle}> Subscriptions </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon color='#3c3c3c' name="folder" size={21} />
          <Text style={styles.tabTitle}> Library </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  navBar: {
    height: 55,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "space-between",
    marginTop: Constants.statusBarHeight
  },
  rightNav: {
    flexDirection: 'row'
  },
  navItem: {
    marginLeft: 25,
    color: '#3c3c3c'
  },
  body: {
    flex: 1
  },
  tabBar: {
    backgroundColor: 'white',
    height: 51,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around'
  }, 
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabTitle: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 1
  },
  tabTitleClicked: {
    fontSize: 11,
    color: 'red',
    paddingTop: 1
  }
});
