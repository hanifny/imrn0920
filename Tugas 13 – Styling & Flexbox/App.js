import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList } from 'react-native';
import Constants from 'expo-constants';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Register from './components/register';
import Login from './components/login';
import About from './components/about';

export default function App() {
  return (
    // <About />
    <Login />
    // <Register />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  navBar: {
    height: 55,
    backgroundColor: 'gold',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "space-between",
    marginTop: Constants.statusBarHeight
  },
  body: {
    flex: 1,
    paddingHorizontal: 7,
    paddingVertical: 21
  },
  tabBar: {
    height: 51,
    flexDirection: 'row',
    justifyContent: 'center'
  }, 
});
