// No. 1 Looping While 
let i = 2
console.log("LOOPING PERTAMA")
while(i <= 20) {
    console.log(i + " - " + "I love coding")
    i+=2
}
i = 20
console.log("\nLOOPING KEDUA")
while(i >= 2) {
    console.log(i + " - " + "I will become a mobile developer")
    i-=2
}
// No. 2 Looping menggunakan for
console.log("\nOUTPUT");
for(i = 1; i <= 20; i++) {
    if(i % 2 === 1) {
        if(i % 3 === 0) {
            console.log(i + " - I Love Coding")
        } else {
            console.log(i + " - Santai")
        }
    } else if(i % 2 === 0) {
        console.log(i + " - Berkualitas")
    } 
}
// No. 3 Membuat Persegi Panjang
let persegi_panjang = ""
for(let i = 1; i <= 4; i++) {
    for(let j = 1; j <= 8; j++) {
        persegi_panjang += "#"
    }
    persegi_panjang += "\n"
}
console.log("\n" + persegi_panjang)
// No. 4 Membuat Tangga 
let tangga = ""
for(let i = 1; i <= 7; i++) {
    for(let j = 1; j <= i; j++) {
        tangga += "#"
    }
    tangga += "\n"
}
console.log("\n" + tangga)
// No. 4 Membuat Papan Catur
let papan_catur = ""
for(let i = 1; i <= 8; i++) {
    for(let j = 1; j <= 8; j++) {
        if(i % 2 === 1) {
            if(j % 2 === 0) {
                papan_catur += "#"
            } else {
                papan_catur += " "            
            }
        } else if(i % 2 === 0) {
            if(j % 2 === 0) {
                papan_catur += " "            
            } else {
                papan_catur += "#"
            }
        }
    }
    papan_catur += "\n"
}
console.log("\n" + papan_catur)