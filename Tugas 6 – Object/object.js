// Soal No. 1 (Array to Object)
function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear()
    let people = []
    if(!arr.length) {
        console.log("")
    }
    for(let i = 0; i < arr.length; i++) {
        fullName = arr[i][0] + " " + arr[i][1]                
        people[fullName] = {}
        people[fullName].firstName = arr[i][0]
        people[fullName].lastName = arr[i][1]
        people[fullName].gender = arr[i][2]
        if(typeof arr[i][3] != "undefined" && thisYear >= arr[i][3]) {
            people[fullName].age = thisYear - arr[i][3]
        } else {
            people[fullName].age = "Invalid Birth Year"
        }
        console.log(i + 1 + ". " + fullName + ": " + JSON.stringify(people[fullName]))
    }
}
// Driver code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
// Error case 
arrayToObject([]) // ""
console.log("\n")

// Soal No. 2 (Shopping Time)
let item = [
    {
        nama: "Sepatu Stacattu",
        harga: 1500000
    },
    {
        nama: "Baju Zoro",
        harga: 500000
    },
    {
        nama: "Baju H&N",
        harga: 250000
    },
    {
        nama: "Sweater Uniklooh",
        harga: 175000
    },
    {
        nama: "Casing Handphone",
        harga: 50000
    },
]
function shoppingTime(memberId, money) {
    let dataShopping = {}
    if(memberId === "" || typeof memberId === "undefined") {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if(money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    } else {
        dataShopping.memberId = memberId
        dataShopping.money = money
        dataShopping.listPurchased = []
        for(let i = 0; i < item.length; i++) {
            if (item[i].harga <= money) {
                dataShopping.listPurchased.push(item[i].nama)
                money -= item[i].harga 
            }
        }
        dataShopping.changeMoney = money
        return dataShopping
    }
}   
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000))
console.log(shoppingTime('', 2475000)) 
console.log(shoppingTime('234JdhweRxa53', 15000)) 
console.log(shoppingTime()) 
console.log("\n")

// Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let penumpang = []
    if(arrPenumpang.length) {
        for (let i = 0; i < arrPenumpang.length; i++) {
            let data = {}
            data.penumpang = arrPenumpang[i][0]
            data.naikDari = arrPenumpang[i][1]
            data.tujuan = arrPenumpang[i][2]
            data.bayar = 0
            for (let j = 0; j < rute.length; j++) {
                if(data.naikDari === rute[j]) {
                    for (let k = j; k < rute.length; k++) {
                        if (data.tujuan === rute[k]) {
                            break
                        } else {
                            data.bayar += 2000
                        }
                    }
                }
            }            
            penumpang[i] = data
        }
        return penumpang
    } else {
        return []
    }
}
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
console.log(naikAngkot([]))