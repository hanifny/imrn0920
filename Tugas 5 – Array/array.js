// Soal No. 1 (Range)
function range(a, b) {
    let c = []
    if (b) {
        if (b > a) {
            while (a <= b) {
                c.push(a)
                a++
            }
            return c
        } else {
            while (a >= b) {
                c.push(a)
                a--
            }
            return c
        }
    } else {
        return -1
    }
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("\n")

// Soal No. 2 (Range with Step)
function rangeWithStep(a, b, step) {
    let c = []
    if (b) {
        if (b > a) {
            while (a <= b) {
                c.push(a)
                a += step
            }
            return c
        } else {
            while (a >= b) {
                c.push(a)
                a -= step
            }
            return c
        }
    } else {
        return -1
    }
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]
console.log("\n")

// Soal No. 3 (Sum of Range)
function sum(a, b, step) {
    let sumResult = 0
    let result
    if (typeof a === 'undefined') {
        return 0
    } else if (typeof b === 'undefined') {
        return 1
    } else if (typeof step === 'undefined') {
        result = range(a, b)
        for (let i = 0; i < result.length; i++) {
            sumResult += result[i]
        }
        return sumResult
    } else if (typeof step !== 'undefined') {
        result = rangeWithStep(a, b, step)
        for (let i = 0; i < result.length; i++) {
            sumResult += result[i]
        }
        return sumResult
    }
}
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0
console.log("\n")
// Soal No. 4 (Array Multidimensi)
function dataHandling(input) {
    for (let i = 0; i < input.length; i++) {
        console.log("NOMOR ID: " + input[i][0])
        console.log("Nama Lengkap: " + input[i][1])
        console.log("TTL: " + input[i][2] + " " + input[i][3])
        console.log("Hobi: " + input[i][4])
        console.log("\n")
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
dataHandling(input)
// Soal No. 5 (Balik Kata)
function balikKata(string) {
    return string.split("").reverse().join("")
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("\n")
// Soal No. 6 (Metode Array)
function dataHandling2(x) {
    let input = x.splice(",")
    let bulan = input[3].split("/")
    daftar_bulan = [
        "Januari", "Februari", "Maret",
        "April", "Mei", "Juni",
        "Juli", "Agustus", "September",
        "Oktober", "November", "Desember"
    ]
    switch (true) {
        case 0 < bulan[1] && bulan[1] < 13:
            bulan = daftar_bulan[bulan[1] - 1] 
            break
        default:
            bulan = "Input tidak valid!"
            break
    }
    let tbt = input[3].split("/").sort(function (value1, value2) { return value2 - value1 } )
    let tbt2 = input[3].split("/").join("-")
    let nama = String(input[1]).slice(0,15)
    console.log(bulan)
    console.log(tbt)
    console.log(tbt2)
    console.log(nama)
}
var input = ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
dataHandling2(input)